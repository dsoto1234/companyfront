//  import Vue from 'vue'
import axios from 'axios'

const client = axios.create({
  baseURL: 'http://localhost:8000/api',
  json: true
})

const getAuthKeysCall = async () => {
  try {
    return await axios.post('http://localhost:8000/api/login',
      {email: 'admin@admin.com', password: 'password'})
  } catch (error) {
    console.error(error)
  }
}

const getToken = async () => {
  return getAuthKeysCall().then(response => {
    if (response.data.data.api_token) {
      return response.data.data.api_token
    }
  }).catch(error => {
    console.log(error)
  })
}

export default {
  async execute (method, resource, data) {
    // inject the accessToken for each request
    let accessToken = await getToken()
    return client({
      method,
      url: resource,
      data,
      headers: {
        Authorization: `Bearer ${accessToken}`,
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(req => {
      return req.data
    }).catch(error => {
      console.log(error)
    })
  },
  getCompanies () {
    return this.execute('get', '/company')
  },
  getCompany (id) {
    return this.execute('get', `/company/${id}`)
  },
  createCompany (data) {
    return this.execute('post', '/company', data)
  },
  updateCompany (id, data) {
    return this.execute('put', `/company/${id}`, data)
  },
  deleteCompany (id) {
    return this.execute('delete', `/company/${id}`)
  },
  checkCompanyDeletion (id) {
    return this.execute('post', `/company/check/${id}`)
  },
  getEmployees () {
    return this.execute('get', '/employee')
  },
  getEmployee (id) {
    return this.execute('get', `/employee/${id}`)
  },
  createEmployee (data) {
    return this.execute('post', '/employee', data)
  },
  updateEmployee (id, data) {
    return this.execute('put', `/employee/${id}`, data)
  },
  deleteEmployee (id) {
    return this.execute('delete', `/employee/${id}`)
  }
}
